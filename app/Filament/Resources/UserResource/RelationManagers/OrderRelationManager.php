<?php


namespace App\Filament\Resources\UserResource\RelationManagers;

use Livewire\Component;
use Filament\Resources\RelationManagers\RelationManager;

class OrderRelationManager extends Component
{
    public function render()
    {
        return view('livewire.order-relation-manager');
    }
}
