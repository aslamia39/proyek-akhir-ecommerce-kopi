<?php

namespace App\Filament\Widgets;

use App\Filament\Resources\OrderResource;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Actions\Action;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;
use Illuminate\Database\Eloquent\Model;

class LatestOrders extends BaseWidget 
{
    protected int | string | array $columnSpan = 'full';

    protected static ?int $sort = 2;

    public function table(Table $table): Table
    {
        return $table
            ->query(OrderResource::getEloquentQuery())
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns([
                TextColumn::make('id')
                    ->label('Order ID')
                    ->searchable(),

                TextColumn::make('user.name')
                    ->searchable(),

                TextColumn::make('grand_total')
                    ->label('Total Keseluruhan')
                    ->money('IDR'),

                TextColumn::make('status')
                    ->label('Status Pengiriman')
                    ->badge()
                    ->color(fn (string $state):string => match($state){
                        'new' => 'info',
                        'processing' => 'warning',
                        'shipped' => 'success',
                        'delivered' => 'success',
                        'cancelled' => 'danger'
                })
                ->icon(fn (string $state):string => match($state){
                    'new' => 'heroicon-m-sparkles',
                    'processing' => 'heroicon-m-arrow-path',
                    'shipped' => 'heroicon-m-truck',
                    'delivered' => 'heroicon-o-check-badge',
                    'cancelled' => 'heroicon-m-x-circle'
                })
                ->sortable(),

            TextColumn::make('payment_method')
                ->label('Metode Pembayaran')
                ->sortable()
                ->searchable(),

            TextColumn::make('payment_status')
                ->label('Status Pembayaran')
                ->sortable()
                ->badge()
                ->searchable(),

            TextColumn::make('created_at')
                ->label('Order Date')
                ->dateTime(),
            ])
            ->actions([
                Action::make('View Order')
                ->url(fn (Model $record): string => OrderResource::getUrl('view', ['record' => $record]))
                ->color('info')
                ->icon('heroicon-m-eye'),
            ]);
    }
}
